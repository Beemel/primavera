package com.example.primavera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimaVeraApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimaVeraApplication.class, args);
    }

}
