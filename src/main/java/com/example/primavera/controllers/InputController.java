package com.example.primavera.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class InputController {

    @GetMapping("/greeting")
    public String greetingReply() {
        return "Hello there, ma name be h4xx0r5 B0bBy";
    }

    @GetMapping("/greeting/{name}")
    public String greetingFromPathInput(@PathVariable String name) {
        return "G'day to you " + name + ". That is one fine and dandy name you got there, ma name be h4xx0r5 B0bBy. And i am an AU, an Artifical Uintelligence";
    }

    @GetMapping("/palindrome")
    public String isItAPalindrome(@RequestParam String word) {
        palindromeTest(word);

        return palindromeTest(word);
    }

    public String palindromeTest(String word) {
        String originalInput, InputReversed = "";
        originalInput = word;
        int length = originalInput.length();
        for (int i = length - 1; i >= 0; i--) {
            InputReversed = InputReversed + originalInput.charAt(i);
        }
        if (originalInput.equals(InputReversed))
            return (word + " is a palindrome.");
        else return (word + " isn't a palindrome.");

    }
}


